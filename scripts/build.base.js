const { pnpPlugin } = require('@yarnpkg/esbuild-plugin-pnp');

module.exports = (options) =>
  require("esbuild")
    .build({
      bundle: true,
      plugins: [pnpPlugin()],
      loader: {
        [".html"]: "text",
      },
      external: ["vscode"],
      ...options,
    })
    .catch(() => process.exit(1));
