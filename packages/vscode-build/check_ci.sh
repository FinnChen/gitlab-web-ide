#!/usr/bin/env bash
if [[ "$CI" == "true" ]] && [[ -d "./dist/vscode" ]]
then
  echo "[check_ci] dist/vscode already exists from CI cache. Skipping..."
  exit 0
else
  exit 1
fi
