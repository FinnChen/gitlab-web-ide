# @gitlab/vscode-build

This package is used to provide a web build of vscode.

The web build itself is **not** checked into source control. Instead, we
provide the location of the build in `vscode_version.json`.
