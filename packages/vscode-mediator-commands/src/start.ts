import { Config } from '@gitlab/web-ide-types';
import {StartCommandResponse} from './types'
import {GitLabClient} from '@gitlab/gitlab-api-client'

export const handleStartCommand = async (config: Config, client: GitLabClient): Promise<StartCommandResponse> => {
  const branch = await client.fetchProjectBranch(config.projectPath, config.ref);

  const files = await client.fetchFiles(config.projectPath, branch.commit.id);

  return {
    branch,
    files,
  };
};
