import {gitlab} from '@gitlab/gitlab-api-client'

export interface StartCommandResponse {
  files: string[],
  branch: gitlab.Branch,
}

export interface ICommand {
  id: string,
  handler: (...args: any[]) => unknown
}

export interface VSCodeBuffer {
  readonly buffer: Uint8Array,
}

export type VSBufferWrapper = (arg0: Uint8Array) => VSCodeBuffer;
