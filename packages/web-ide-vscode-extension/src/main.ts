import * as vscode from 'vscode';
import { createFileSystem } from './FileSystem';
import { GitLabFileSystemProvider } from './vscode/GitLabFileSystemProvider';
import { GitLabFileSearchProvider } from './vscode/GitLabFileSearchProvider';
import { start, ready } from './mediator/commands';
import { GitLabFileLoader } from './GitLabFileLoader';
import { FileSearcher } from './FileSearcher';
import { setupSCM } from './vscode/GitLabSourceControl';
import { Git } from './git/Git';

const fetchContextTask = (context: vscode.ExtensionContext) => async (progress: vscode.Progress<{increment: number, message: string}>, token: vscode.CancellationToken) => {
  const result = await start();

  // Register file system stuff...
  const fs = createFileSystem(result.files);
  const loader = new GitLabFileLoader(result.branch.commit.id);
  const git = new Git();
  git.init(fs);

  vscode.workspace.registerFileSystemProvider(
    'gitlab-vscode-ide',
    new GitLabFileSystemProvider(fs, loader),
    {
      isCaseSensitive: true,
      isReadonly: false,
    }
  );
  vscode.workspace.registerFileSearchProvider("gitlab-vscode-ide", new GitLabFileSearchProvider(new FileSearcher(fs)));

  setupSCM(fs, git);


  // Setup commands
  vscode.commands.registerCommand("gitlab-vscode.startCommit", () => {
    vscode.commands.executeCommand('workbench.view.scm');
  });
  
  // Setup views...
  vscode.window.registerTreeDataProvider("gitlab-vscode.commitWelcome", {
    getChildren() { return []; },
    getTreeItem: function (element: any) {
      throw new Error('Function not implemented.');
    }
  });

  // Refresh file view...
  vscode.commands.executeCommand('workbench.action.closeSidebar');
  vscode.commands.executeCommand('workbench.explorer.fileView.focus');

  await ready();
}

export function activate(context: vscode.ExtensionContext) {
  console.log('HELLLOOO FROM GITLAB WORKBENCH!!!!');

  console.log(context.extensionUri);

  vscode.commands.executeCommand('workbench.action.closeAllEditors');
  
  vscode.window.withProgress({
    cancellable: false,
    location: vscode.ProgressLocation.Notification,
    title: 'Initializing GitLab Web IDE...',
  }, fetchContextTask(context));
};

export function deactivate() {

};