import { IFileLoader } from './types';
import { fetchFileRaw } from './mediator/commands';
import { cleanLeadingSeparator } from './utils';

export class GitLabFileLoader implements IFileLoader {
  private readonly _ref: string;

  constructor(ref: string) {
    this._ref = ref;
  }

  async getContent(path: string): Promise<Uint8Array> {
    const vsbuffer = await fetchFileRaw(this._ref, cleanLeadingSeparator(path));

    return vsbuffer.buffer;
  }
}