import * as vscode from 'vscode';
import { FileSystem } from '../FileSystem';
import { GitObjectRepo } from '../git/types';
import { IGit, IFileStatus, FileStatusType } from '../types';
import { GitObjectFileSystem } from './GitObjectFileSystem';

const statusTypeToBadge = (type: FileStatusType) => {
  switch(type) {
    case FileStatusType.Modified:
      return 'M';
    case FileStatusType.Deleted:
      return 'D';
    case FileStatusType.Created:
      return 'A';
    default: 
      return '';
  }
};

const statusToResourceDecoration = (status: IFileStatus): vscode.FileDecoration => {
  return {
    badge: statusTypeToBadge(status.type),
  };
};

const gitObjectIdToUri = (objId: string, contentKey?: string): vscode.Uri => {
  const query = contentKey && `contentKey=${contentKey}`;

  return vscode.Uri.from({
    scheme: 'gl-git-objects',
    path: `/${objId}`,
    query,
  });
};

const statusToCommand = (status: IFileStatus): vscode.Command => {
  const title = status.path;

  if (!(status.origObjId && status.newObjId)) {
    return {
      command: 'vscode.open',
      title: "Open",
      // NOTE: We should be guaranteed that either origObjId or newObjId exist
      arguments: [gitObjectIdToUri(status.origObjId || status.newObjId || ''), { override: false }, title],
    };
  } else {
    return {
      command: 'vscode.diff',
      title: "Open",
      arguments: [gitObjectIdToUri(status.origObjId, status.origContentKey), gitObjectIdToUri(status.newObjId), title]
    };
  }
}

const statusToResourceState = (status: IFileStatus): GitResourceState => {
  return {
    resourceUri: vscode.Uri.from({ scheme: 'gl-git-objects', path: status.path }),
    resourceDecoration: statusToResourceDecoration(status),
    command: statusToCommand(status),
  };
}

interface GitResourceState extends vscode.SourceControlResourceState {
  resourceDecoration: vscode.FileDecoration
}

class GitDecorationProvider implements vscode.FileDecorationProvider {
  private readonly _onDidChangeDecorations = new vscode.EventEmitter<vscode.Uri[]>();
  private decorations = new Map<string, vscode.FileDecoration>();

  public readonly onDidChangeFileDecorations: vscode.Event<vscode.Uri[]> = this._onDidChangeDecorations.event;

  provideFileDecoration(uri: vscode.Uri, token: vscode.CancellationToken): vscode.ProviderResult<vscode.FileDecoration> {
    return this.decorations.get(uri.toString());
  }

  update(states: GitResourceState[]) {
    const newDecorations = new Map<string, vscode.FileDecoration>();

    states.forEach(state => {
      newDecorations.set(state.resourceUri.toString(), state.resourceDecoration);
    });

    this.decorations = newDecorations;
    this._onDidChangeDecorations.fire(states.map(x => x.resourceUri));
  }
}

export const setupSCM = (fs: FileSystem, git: IGit & GitObjectRepo) => {
  const decorationProvider = new GitDecorationProvider();

  vscode.window.registerFileDecorationProvider(decorationProvider);

  const sourceControl = vscode.scm.createSourceControl('gl-git', 'Git');
  sourceControl.acceptInputCommand = {
    command: 'gitlab-vscode.commit',
    title: 'Commit',
    arguments: [],
  };
  sourceControl.inputBox.placeholder = "Commit message...";

  const stagedGroup = sourceControl.createResourceGroup('staged', 'Staged Changes');
  const changesGroup = sourceControl.createResourceGroup('changes', 'Changes');

  stagedGroup.hideWhenEmpty = true;

  vscode.workspace.registerFileSystemProvider("gl-git-objects", new GitObjectFileSystem(git), { isCaseSensitive: true, isReadonly: true })

  vscode.commands.registerCommand("gitlab-vscode.gitRefresh", () => {
    git.update(fs);
    const status = git.status();
    const resourceStates = status.map(statusToResourceState);
    console.log(resourceStates.map(x => ({ uri: x.resourceUri.toString(), text: x.resourceDecoration.badge })));
    changesGroup.resourceStates = resourceStates;
    decorationProvider.update(resourceStates);
  });

  vscode.commands.registerCommand("gitlab-vscode.gitClean", () => {
    git.clean();
  });

  fs.onChange(async () => {
    await vscode.commands.executeCommand("gitlab-vscode.gitRefresh");
    await vscode.commands.executeCommand("gitlab-vscode.gitClean");
  })
};
