import { Uri, FileStat, FileType, FileSystemError } from "vscode";
import { GitBlobObject, GitObjectRepo, GitObjectType, GitTreeObject } from "../git/types";
import { cleanLeadingSeparator } from "../utils/path";
import { ReadOnlyFileSystem } from "./ReadOnlyFileSystem";


const getFileType = (type: GitObjectType) => {
  switch(type) {
    case GitObjectType.BLOB:
      return FileType.File;
    case GitObjectType.TREE:
      return FileType.Directory;
    default:
      return FileType.Unknown;
  }
};

const getObjectId = (uri: Uri) => {
  return cleanLeadingSeparator(uri.path);
};

export class GitObjectFileSystem extends ReadOnlyFileSystem {
  private readonly _git: GitObjectRepo;

  constructor(gitRepo: GitObjectRepo) {
    super();
    this._git = gitRepo;
  }

  stat(uri: Uri): FileStat | Thenable<FileStat> {
    const objId = getObjectId(uri);

    console.log('[GitObjectFileSystem...] path', objId);

    const obj = this._git.readObject(objId);

    if (!obj) {
      throw FileSystemError.FileNotFound(uri);
    }

    return {
      // TODO: Do we need these values??
      ctime: 1,
      mtime: 1,
      size: 1,
      type: getFileType(obj.type),
    };
  }

  readDirectory(uri: Uri): [string, FileType][] | Thenable<[string, FileType][]> {
    const id = getObjectId(uri);

    const obj = this._git.readObject(id);

    if (!obj) {
      throw FileSystemError.FileNotFound(uri);
    }
    if (obj.type !== GitObjectType.TREE) {
      throw FileSystemError.FileNotADirectory(uri);
    }

    const tree = obj as GitTreeObject;

    return tree.data.children.map(x => [x.name, getFileType(x.type)]);
  }

  readFile(uri: Uri): Uint8Array | Thenable<Uint8Array> {
    const id = getObjectId(uri);

    const obj = this._git.readObject(id);

    if (!obj) {
      throw FileSystemError.FileNotFound(uri);
    }
    if (obj.type !== GitObjectType.BLOB) {
      throw FileSystemError.FileIsADirectory(uri);
    }

    const blob = obj as GitBlobObject;

    // TODO: fix this... DELETE will probably not work....
    if (blob.data.content) {
      return blob.data.content;
    } 
    if (!blob.data.hasChangedFromLoaded) {
      const [key, value] = uri.query.split('=');
      
      // TODO - This used to do getCache stuff...
      const content = value && '';
      
      if (content) {
        return content;
      }
    }

    return new Uint8Array([]);
  }
}