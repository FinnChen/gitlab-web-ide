import { CancellationToken, FileSearchOptions, FileSearchProvider, FileSearchQuery, ProviderResult, Uri } from "vscode";
import { IFileSearcher } from "../types";

export class GitLabFileSearchProvider implements FileSearchProvider {
  private readonly _searcher: IFileSearcher;

  constructor(searcher: IFileSearcher) {
    this._searcher = searcher;
  }

  provideFileSearchResults(query: FileSearchQuery, options: FileSearchOptions, token: CancellationToken): ProviderResult<Uri[]> {
    if (!query.pattern) {
      return [];
    }

    // TODO: What about the other options???
    return this._searcher.searchBlobPaths(query.pattern, options.maxResults).map(x => Uri.parse(x));
  }
}