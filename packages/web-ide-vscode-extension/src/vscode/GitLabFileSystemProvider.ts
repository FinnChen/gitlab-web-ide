import { Disposable, FileChangeEvent, FileStat, FileSystemProvider, FileType, Uri, EventEmitter, FileSystemError} from "vscode";
import { FileEntry, IFileSystem, IFileLoader } from "../types";

const getFileStatType = (entry: FileEntry): FileType => {
  switch(entry.type) {
    case "blob":
      return FileType.File;
    case "tree":
      return FileType.Directory;
    default:
      return FileType.Unknown;
  }
};

const toFileStat = (entry: FileEntry): FileStat => ({
  ctime: entry.ctime,
  mtime: entry.mtime,
  size: entry.size,
  type: getFileStatType(entry),
});

export class GitLabFileSystemProvider implements FileSystemProvider {
  private readonly _onDidChangeFileEmitter: EventEmitter<FileChangeEvent[]>;
  private readonly _fs: IFileSystem;
  private readonly _fileLoader: IFileLoader;

  constructor(fs: IFileSystem, fileLoader: IFileLoader) {
    // TODO: We need to actually trigger this...
    this._onDidChangeFileEmitter = new EventEmitter<FileChangeEvent[]>();
    this._fs = fs;
    this._fileLoader = fileLoader;
  }

  get onDidChangeFile() {
    return this._onDidChangeFileEmitter.event;
  }

  watch(uri: Uri, options: { recursive: boolean; excludes: string[]; }): Disposable {
    return {
      dispose: () => null,
    }
  }

  stat(uri: Uri): FileStat | Thenable<FileStat> {
    const entry = this._fs.getEntry(uri.path);

    if (!entry) {
      throw FileSystemError.FileNotFound(uri);
    }

    return toFileStat(entry);
  }

  readDirectory(uri: Uri): [string, FileType][] | Thenable<[string, FileType][]> {
    const entry = this._fs.getEntry(uri.path);

    if (!entry) {
      throw FileSystemError.FileNotFound(uri);
    } else if (entry.type !== "tree") {
      throw FileSystemError.FileNotADirectory(uri);
    }

    const children = entry.children || [];

    return children.map(key => {
      const child = this._fs.getEntry(key)!;

      return [child.name, getFileStatType(child)];
    });
  }

  createDirectory(uri: Uri): void | Thenable<void> {
    if (this._fs.getEntry(uri.path)) {
      throw FileSystemError.FileExists(uri.path);
    }

    this._fs.createDirectory(uri.path);
  }

  async readFile(uri: Uri): Promise<Uint8Array> {
    // We might need to load the content, so we're using "let" instead of "const"
    let entry = this._fs.getEntry(uri.path);

    if (!entry) {
      throw FileSystemError.FileNotFound(uri);
    } else if (entry.type !== "blob") {
      throw FileSystemError.FileIsADirectory(uri);
    }

    // TODO - Same question as before... Do we need to check content here? Can content change without loaded?
    // like if we copy / paste a whole directory ontop of another without loaded the directory???
    if (entry.content?.type === 'unloaded') {
      const content = await this._fileLoader.getContent(entry.content.path);

      entry = this._fs.saveLoadedContent(uri.path, content);
    }

    if (entry.content?.type === 'raw') {
      return entry.content.raw || new Uint8Array([]);
    }

    throw new Error(`[gitlab/web-ide] Unexpected entry content type ${entry.content?.type}`);
  }

  async writeFile(uri: Uri, content: Uint8Array, options: { create: boolean; overwrite: boolean; }): Promise<void> {
    const existingEntry = this._fs.getEntry(uri.path);

    if (!existingEntry) {
      if (!options.create) {
        throw FileSystemError.FileNotFound(uri);
      }

      this._fs.createFile(uri.path, content);
    } else {
      if (!options.overwrite) {
        throw FileSystemError.FileExists(uri);
      }

      this._fs.writeFile(uri.path, content);
    }
  }

  delete(uri: Uri, options: { recursive: boolean; }): void | Thenable<void> {
    const existingEntry = this._fs.getEntry(uri.path);

    if (!existingEntry) {
      throw FileSystemError.FileNotFound(uri);
    }
    if (!options.recursive && existingEntry.type === "tree") {
      throw FileSystemError.FileIsADirectory(uri);
    }

    this._fs.delete(uri.path);
  }

  rename(oldUri: Uri, newUri: Uri, options: { overwrite: boolean; }): void | Thenable<void> {
    if (!options.overwrite && this._fs.getEntry(newUri.path)) {
      throw FileSystemError.FileExists(newUri);
    }
    if (!this._fs.getEntry(oldUri.path)) {
      throw FileSystemError.FileNotFound(oldUri);
    }

    this._fs.rename(oldUri.path, newUri.path);
  }

  copy(source: Uri, target: Uri, options?: { overwrite: boolean }): void | Thenable<void> {
    console.log("Copying...", source.path, target.path);

    if (!options?.overwrite && this._fs.getEntry(target.path)) {
      throw FileSystemError.FileExists(target);
    }
    if (!this._fs.getEntry(source.path)) {
      throw FileSystemError.FileNotFound(source);
    }
    if (!this._fs.getParentEntry(source.path)) {
      throw FileSystemError.FileNotFound(source.path);
    }

    this._fs.copy(source.path, target.path);
  }
}
