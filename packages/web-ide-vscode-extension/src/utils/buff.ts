// TODO - is there a more native way to convert Uint8Array => number[]
export const arrayBufferToString = (buff: ArrayBuffer) => String.fromCharCode.apply(null, (new Uint8Array(buff) as any));
