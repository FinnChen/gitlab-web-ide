import { FileEntry, MutableFileEntry } from '../types';
import { sha256 } from './sha256';

export const cloneFileEntry = (f: FileEntry): MutableFileEntry => {
  return {
    ...f,
  };
};

export const loadFileContent = (f: MutableFileEntry, content: Uint8Array) => {
  // TODO: Do we want to just no-op if we're already loaded??
  if (f.content?.type !== 'unloaded') {
    return;
  }

  f.content = {
    type: 'raw',
    raw: content,
  };
  f.initContentHash = sha256(content);

  // console.log('[loadFileContent]', f);
};
