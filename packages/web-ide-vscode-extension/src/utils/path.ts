export const PATH_SEPARATOR = '/';
const PATH_SEPARATOR_LEADING_REGEX = new RegExp(`^${PATH_SEPARATOR}+`);
const PATH_SEPARATOR_ENDING_REGEX = new RegExp(`${PATH_SEPARATOR}+$`);

export const splitParent = (path: string): [string | null, string] => {
  const idx = path.lastIndexOf('/');

  return [
    // parent
    idx >= 0 ? path.substring(0, idx) : null,
    // name
    idx >= 0 ? path.substring(idx + 1) : path,
  ];
};

export const startWithSlash = (path: string): string => path[0] === '/' ? path : `/${path}`;

export function cleanLeadingSeparator(path: string): string {
  return path.replace(PATH_SEPARATOR_LEADING_REGEX, '');
}

function cleanEndingSeparator(path: string): string {
  return path.replace(PATH_SEPARATOR_ENDING_REGEX, '');
}

// TODO: improve this
/**
 * Safely joins the given paths which might both start and end with a `/`
 *
 * Example:
 * - `joinPaths('abc/', '/def') === 'abc/def'`
 * - `joinPaths(null, 'abc/def', 'zoo) === 'abc/def/zoo'`
 *
 * @param  {...String} paths
 * @returns {String}
 */
export function joinPaths(...paths: string[]): string {
  return paths.reduce((acc, path) => {
    if (!path) {
      return acc;
    }
    if (!acc) {
      return path;
    }

    return [cleanEndingSeparator(acc), PATH_SEPARATOR, cleanLeadingSeparator(path)].join('');
  }, '');
}
