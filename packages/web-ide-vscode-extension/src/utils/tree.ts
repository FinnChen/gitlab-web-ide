import { GitObjectType } from "../git/types";

export const getTreeChildSortKey = ({ type, name }: { type: GitObjectType, name: string }) => `${type === GitObjectType.TREE ? 0 : 1} ${name}`;
