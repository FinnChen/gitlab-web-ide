import { EventEmitter } from "vscode";
import { getFileSystemEntries, ensureDirectory, insertBlob, ROOT_PATH } from "./fsEntries";
import { MutableFileEntry, FileEntry, IFileSystem } from './types';
import { cloneFileEntry, loadFileContent } from "./utils/fileEntry";
import { joinPaths, splitParent } from "./utils/path";

const getModTime = () => Date.now();

export class FileSystem implements IFileSystem {
  private readonly _entries: Map<string, MutableFileEntry>;
  private readonly _onChangeEmitter: EventEmitter<void>;

  constructor(initEntries: Map<string, MutableFileEntry>) {
    this._onChangeEmitter = new EventEmitter<void>();
    this._entries = initEntries;
  }

  get onChange() {
    return this._onChangeEmitter.event;
  }

  getEntry(path: string): FileEntry | undefined {
    return this._entries.get(path);
  }

  saveLoadedContent(path: string, content: Uint8Array): FileEntry {
    const entry = this._entries.get(path);

    if (!entry) {
      throw new Error(`[gitlab/web-ide] could not find entry at path ${path}`);
    }

    // We shouldn't have to update mod time because technically nothing has changed
    loadFileContent(entry, content);

    return entry;
  }

  writeFile(path: string, content: Uint8Array): void {
    const entry = this._entries.get(path);

    if (!entry) {
      return;
    }

    // TODO - Can we write to a file that isn't loaded yet? Do we need to loaded = true here?
    entry.content = {
      type: 'raw',
      raw: content,
    };

    this._updateModTime(path, getModTime());
  }

  createFile(path: string, content?: Uint8Array): void {
    if (this._entries.has(path)) {
      throw new Error(`Cannot create new file. Path already found: ${path}`);
    }

    const entry = insertBlob(this._entries, path);    

    loadFileContent(entry, content || new Uint8Array([]));
  }

  createDirectory(path: string): void {
    if (this._entries.has(path)) {
      throw new Error(`Cannot create new directory. Path already found: ${path}`);
    }
    
    ensureDirectory(this._entries, path);
  }

  delete(path: string): void {
    if (!this._entries.has(path)) {
      return;
    }

    this._delete(path, true);
  }

  getBlobPaths(): string[] {
    const paths: string[] = [];

    this._entries.forEach((value, key) => {
      if (value.type === 'blob') {
        paths.push(key);
      }
    });

    return paths;
  }

  private _delete(path: string, withModTime: boolean) {
    // We've already checked for existence in the public delete()
    const entry = this._entries.get(path)!;

    if (entry.type === "tree") {
      entry.children?.forEach(childPath => {
        this._delete(childPath, false);
      });
    }

    const parentPath = this._getParentPath(path);
    // We can assume that the parent exists (parentPath will fallback to ROOT)
    const parent = this._entries.get(parentPath)!;

    this._removeChild(parent, path);
    this._entries.delete(path);

    if (withModTime) {
      this._updateModTime(parentPath, getModTime());
    }
  }

  rename(oldPath: string, newPath: string): void {
    if (oldPath === ROOT_PATH) {
      throw new Error(`Cannot rename. Old path cannot be root directory.`);
    }
    if (newPath === ROOT_PATH) {
      throw new Error(`Cannot rename. New path cannot be root directory.`);
    }
    if (!this._entries.has(oldPath)) {
      throw new Error(`Cannot rename. Old path is not found: ${oldPath}`);
    }

    // TODO rollback if something bad happens...
    this.delete(newPath);
    const mtime = getModTime();
    this._rename(oldPath, newPath, mtime);
    this._renameParentsChildPath(oldPath, newPath);

    // update parents mod time. the children are taken care of inside the _rename.
    this._updateModTime(newPath, mtime);
  }

  private _rename(oldPath: string, newPath: string, mtime: number) {
    // We can assume oldPath exists. This is already checked in the public rename() method
    const oldEntry = this._entries.get(oldPath)!;

    const [,name] = splitParent(newPath);

    const newEntry: FileEntry = {
      ...oldEntry,
      name,
      mtime,
      children: oldEntry.children?.map(childPath => {
        const [,childName] = splitParent(childPath);

        return joinPaths(newPath, childName);
      })
    };

    if (oldEntry.type === 'tree' && oldEntry.children) {
      oldEntry.children.forEach(childPath => {
        const [,childName] = splitParent(childPath);

        this._rename(childPath, joinPaths(newPath, childName), mtime);
      });                  
    }

    this._entries.delete(oldPath);
    this._entries.set(newPath, newEntry);
  }

  private _renameParentsChildPath(oldChildPath: string, newChildPath: string) {
    const oldParentEntry = this._getParentEntry(oldChildPath);

    this._removeChild(oldParentEntry, oldChildPath);

    // make sure new parent directory exists... if parent is falsey it'll default to root
    const [parent] = splitParent(newChildPath);

    const dir = ensureDirectory(this._entries, parent);
    dir.children!.push(newChildPath);
  }

  public copy(sourcePath: string, destPath: string) {
    if (!this._entries.has(sourcePath)) {
      throw new Error(`Cannot copy. Path does not exist: ${sourcePath}`);
    }
    if (!this._entries.has(this._getParentPath(destPath))) {
      throw new Error(`Cannot copy. Parent destination path does not exist: ${destPath}`);
    }

    const mtime = getModTime();

    this._cloneEntry(sourcePath, destPath, mtime);

    // update parents mod time. the children are taken care of inside the _cloneEntry
    this._updateModTime(destPath, mtime);
  }

  private _cloneEntry(sourcePath: string, newPath: string, mtime: number) {
    // We can assume sourcePath exists. This is already checked for in the public method.
    const sourceEntry = this._entries.get(sourcePath)!;
    const [,newName] = splitParent(newPath);

    const newEntry = {
      ...cloneFileEntry(sourceEntry),
      mtime,
      newName,
      children: this._cloneEntryChildren(sourceEntry, sourcePath, newPath, mtime),
    };

    this._entries.set(newPath, newEntry);
  }

  private _cloneEntryChildren(sourceEntry: FileEntry, sourcePath: string, newPath: string, mtime: number) {
    const newChildren = sourceEntry.children?.map(childPath => {
      const [,childName] = splitParent(childPath);

      return joinPaths(newPath, childName);
    });

    // ensure the new children paths actually exist...
    newChildren?.forEach(childPath => {
      const [,childName] = splitParent(childPath);

      const childSourcePath = joinPaths(sourcePath, childName);

      this._cloneEntry(childSourcePath, childPath, mtime);
    });

    return newChildren;
  }

  private _updateModTime(path: string, mtime: number) {
    if (!path) {
      return;
    }

    const entry = this._entries.get(path);

    if (!entry) {
      return;
    }

    entry.mtime = mtime;

    const parentPath = this._getParentPath(path);

    // Avoid recursive call when path is root
    if (parentPath === path) {
      // TODO: Is this the best place to emit that we have changed??
      this._onChangeEmitter.fire();
      return;
    }

    this._updateModTime(parentPath, mtime);
  }

  private _getParentPath(path: string): string {
    const [parent] = splitParent(path);    

    return parent || ROOT_PATH;
  }

  public getParentEntry(path: string): FileEntry {
    return this._getParentEntry(path);
  }

  private _getParentEntry(path: string): FileEntry {
    // We can assume that parent path exists. It falls back to ROOT.
    return this._entries.get(this._getParentPath(path))!;
  }

  private _removeChild(entry: FileEntry, path: string) {
    if (!entry.children) {
      return;
    }

    const idx = entry.children.indexOf(path);

    if (idx >= 0) {
      entry.children.splice(idx, 1);
    }
  }
} 

export const createFileSystem = (files: string[]): FileSystem => {
  const entries = getFileSystemEntries(files);

  return new FileSystem(entries);
};
