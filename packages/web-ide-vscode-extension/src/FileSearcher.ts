import fuzzaldrinPlus from 'fuzzaldrin-plus';
import { ROOT_PATH } from './fsEntries';
import { FileSystem } from './FileSystem';
import { IFileSearcher } from "./types";

export class FileSearcher implements IFileSearcher {
  private readonly _fs: FileSystem;
  private _paths: string[];
  private _lastReadMS: number;

  constructor(fs: FileSystem) {
    this._fs = fs;
    this._paths = [];
    this._lastReadMS = -1;
  }

  searchBlobPaths(term: string, maxResults: number = 0): string[] {
    this._checkForRefresh();

    return fuzzaldrinPlus.filter(this._paths, term, {
      maxResults: maxResults <= 0 ? 10 : maxResults,
    });
  }

  private _checkForRefresh(): void {
    const root = this._fs.getEntry(ROOT_PATH)!;

    if (root.mtime > this._lastReadMS) {
      this._refresh(root.mtime);
    }
  }

  private _refresh(ms: number): void {
    this._paths = this._fs.getBlobPaths();
    this._lastReadMS = ms;
  }
}
