import { MutableFileEntry } from "./types";
import { splitParent, startWithSlash } from "./utils/path";

export const ROOT_PATH = '/';

const insertRoot = (entries: Map<string, MutableFileEntry>) => {
  const root: MutableFileEntry =  { 
    name: ROOT_PATH,
    ctime: 0,
    mtime: 0,
    size: 0,
    type: "tree",
    children: [],
  };

  entries.set(ROOT_PATH, root);

  return root;
};

export const ensureDirectory = (entries: Map<string, MutableFileEntry>, path: string | null): MutableFileEntry => {
  if (!path) {
    return entries.get(ROOT_PATH)!;
  } else if (entries.has(path)) {
    return entries.get(path)!;
  }

  const [parent, name] = splitParent(path);

  const parentDir = ensureDirectory(entries, parent);

  const directory: MutableFileEntry = {
    name,
    ctime: 0,
    mtime: 0,
    size: 0,
    type: "tree",
    children: [],
  };

  entries.set(path, directory);
  parentDir.children?.push(path);

  return directory;
};

export const insertBlob = (entries: Map<string, MutableFileEntry>, path: string): MutableFileEntry => {
  const [parent, name] = splitParent(path);

  const parentDir = ensureDirectory(entries, parent);

  const file: MutableFileEntry = {
    name,
    ctime: 0,
    mtime: 0,
    size: 0,
    type: 'blob',
    content: {
      type: 'unloaded',
      path,
    }
  };

  entries.set(path, file);
  parentDir.children?.push(path);

  return file;
};

// This is very much inspired from:
// https://gitlab.com/gitlab-org/gitlab/blob/00827a74cf3bfef985ed6046fb2d42f29cbb19ac/app/assets/javascripts/ide/lib/files.js
export const getFileSystemEntries = (data: string[]): Map<string, MutableFileEntry> => {
  const entries = new Map<string, MutableFileEntry>();

  insertRoot(entries);
    
  data.map(startWithSlash).forEach((path) => insertBlob(entries, path));

  return entries;
};
