/**
 * Thanks to https://gitlab.com/gitlab-org/gitlab/-/merge_requests/34496/diffs
 */
import { groupBy } from 'lodash';
import { createGitObjectBlob, createGitObjectTree, hashGitObject } from './objects';
import { joinPaths } from '../utils/path';
import { getTreeChildSortKey } from '../utils/tree';
import { GitObject, GitObjectType, GitTreeObjectChild } from './types';
import { FileEntry, IFileSystem } from '../types';
import { ROOT_PATH } from '../fsEntries';

interface ObjectsUpdateContext {
  rootRef: string,
  objects: Map<string, GitObject<any>>,
  fs: IFileSystem,
  lastTimestamp: number
}

const getGitObjectType = (type: string): GitObjectType => {
  if (type === "tree") {
    return GitObjectType.TREE;
  } else {
    return GitObjectType.BLOB;
  }
}

const getTreeChildKey = (context: ObjectsUpdateContext, path: string, treeChild: FileEntry, prevChildren: Record<string, GitTreeObjectChild[]>) => {
  const { lastTimestamp } = context;

  const sortKey = getTreeChildSortKey({ type: getGitObjectType(treeChild.type), name: treeChild.name });
  const prevChild = prevChildren[sortKey];
  const prevKey = prevChild?.[0]?.key;
  const fullPath = joinPaths(path, treeChild.name);

  if (prevKey && lastTimestamp >= context.fs.getEntry(fullPath)!.mtime) {
    return prevKey;
  }

  return updateObjectsWithPath(context, fullPath, prevKey);
};

const getChildrenWithKeys = (context: ObjectsUpdateContext, path: string, ref: string): GitTreeObjectChild[] => {
  const tree = context.fs.getEntry(path);
  const { objects } = context;
  const prevObj = objects.get(ref);
  const prevChildren = prevObj ? groupBy(prevObj.data.children, getTreeChildSortKey) : {};

  if (!tree?.children) {
    return [];
  }

  return tree.children.map(treeChildPath => {
    const treeChildEntry = context.fs.getEntry(treeChildPath)!;

    const { name, type } = treeChildEntry;

    return {
      type: type === 'tree' ? GitObjectType.TREE : GitObjectType.BLOB,
      name,
      key: getTreeChildKey(context, path, treeChildEntry, prevChildren),
    };
  });
};

/**
 *
 * @param {Object} objects
 */
export function updateObjectsWithBlob(objects: Map<string, GitObject<any>>, file: FileEntry) {
  const obj = createGitObjectBlob(file);
  const key = hashGitObject(obj);

  if (!objects.has(key)) {
    objects.set(key, obj);
  }

  return key;
}

function updateObjectsWithTreeChildren(objects: Map<string, GitObject<any>>, children: GitTreeObjectChild[]): string {
  const obj = createGitObjectTree(children);
  const key = hashGitObject(obj);

  if (!objects.has(key)) {
    objects.set(key, obj);
  }

  return key;
}

/**
 *
 * @param {{rootRef: String, objects: Object, fs: Object, lastTimestamp: Number}} context
 */
export function updateObjectsWithTree(context: ObjectsUpdateContext, path: string, ref: string) {
  const childrenWithKeys = getChildrenWithKeys(context, path, ref);

  return updateObjectsWithTreeChildren(context.objects, childrenWithKeys);
}

/**
 *
 * @param {{rootRef: String, objects: Object, fs: Object, lastTimestamp: Number}} context
 */
export function updateObjectsWithPath(context: ObjectsUpdateContext, path: string, baseRef: string) {
  const entry = context.fs.getEntry(path);

  if (!entry) {
    console.error('[ide.git.fs] expected to find entry at', path);
    return '';
  }

  if (entry.type === 'blob') {
    return updateObjectsWithBlob(context.objects, entry);
  }

  return updateObjectsWithTree(context, path, baseRef);
}

/**
 *
 * @param {{rootRef: String, objects: Object, fs: Object, lastTimestamp: Number}} context
 */
export const updateObjects = (context: ObjectsUpdateContext) => {
  return updateObjectsWithTree(context, ROOT_PATH, context.rootRef);
};
