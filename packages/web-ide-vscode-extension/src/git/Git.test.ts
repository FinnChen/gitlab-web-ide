import { Git } from './Git';
import { createFileSystem } from '../FileSystem';
import { GitObjectType } from './types';
import { recursiveReadGitObject } from '../../test-utils/recursiveReadGitObject';
import { IFileSystem } from '../types';

const README_CONTENT = new TextEncoder().encode('Hello world!\nLorem ipsum dolar sit!\n');
const README_CONTENT_NEW = new TextEncoder().encode('Hi!\nLorem ipsum\n');

describe('Git', () => {
  // noinspection SpellCheckingInspection
  const BLANK_BLOB_KEY = '6098c63f046d71e40140de5caed7eb794ef24eabd78e55fc87ceb6138b5173fb';

  const createBlankBlobChild = (name: string) => (
    {
      key: BLANK_BLOB_KEY,
      name,
      type: GitObjectType.BLOB,
      data: {
        content: new Uint8Array(),
        hasChangedFromLoaded: false,
        initContentHash: undefined,
      }
    }
  );
  const createDefaultFileSystem = () => createFileSystem(['README.md', 'CONTRIBUTING.md']);

  describe('init', () => {
    let git: Git;
    let fs: IFileSystem;

    beforeEach(() => {
      fs = createDefaultFileSystem();

      git = new Git();

      git.init(fs);
    });

    it('should have same refs for head and fs', () => {
      expect(git.headRef).toBe(git.fsRef);
    });

    it('should initialize git with fs', () => {
      expect(recursiveReadGitObject(git, git.headRef)).toEqual({
        type: GitObjectType.TREE,
        data: {
          children: [
            createBlankBlobChild('CONTRIBUTING.md'),
            createBlankBlobChild('README.md'),
          ]
        }
      });
    });

    describe('when content is loaded for file', () => {
      let origRef: string;

      beforeEach(() => {
        origRef = git.headRef;

        fs.saveLoadedContent('/README.md', README_CONTENT);
        git.update(fs);
      });

      it('should not change', () => {
        expect(git.headRef).toBe(origRef);
        expect(git.fsRef).toBe(origRef);
      });

      it('should update git objects', () => {
        // console.log(git.readObject(git.headRef));

        expect(recursiveReadGitObject(git, git.headRef)).toEqual({
          type: GitObjectType.TREE,
          data: {
            children: [
              createBlankBlobChild('CONTRIBUTING.md'),
              createBlankBlobChild('README.md'),
            ]
          }
        });
      });
    });

    describe('when new content is written to file', () => {
      let origRef: string;

      beforeEach(() => {
        origRef = git.headRef;

        fs.writeFile('/README.md', README_CONTENT_NEW);
        git.update(fs);
      });

      it('head ref should not change', () => {
        expect(git.headRef).toBe(origRef);
      });

      it('fs ref should change', () => {
        expect(git.fsRef).not.toBe(git.headRef);
      });
    });
  });
});
