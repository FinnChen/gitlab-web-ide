export enum GitObjectType {
  TREE = 1,
  BLOB = 2
};

export interface GitObjectRepo {
  readObject(id: string): GitObject<any> | undefined
}

// TODO: Remove the generic and data prop by adding the
//   GitBlobObjectData properties siblings of `type`.
//   Then we can replace all references to
//   GitTreeObject | GitBlobObject with just GitObject
export interface GitObject<T> {
  readonly type: GitObjectType,
  readonly data: T,
}

// TODO: Remove this alias once we fix the TODO above to make
//  GitObject not take a generic
export type GitTreeOrBlobObject = GitTreeObject | GitBlobObject

export interface GitBlobObject extends GitObject<GitBlobObjectData> {
  readonly type: GitObjectType.BLOB,
}

export interface GitBlobObjectData {
  readonly hasChangedFromLoaded: boolean,
  readonly content: Uint8Array,
  readonly initContentHash?: string,
}

export interface GitTreeObject extends GitObject<GitTreeObjectData> {
  readonly type: GitObjectType.TREE,
}

export interface GitTreeObjectData {
  readonly children: GitTreeObjectChild[],
}

export interface GitTreeObjectChild {
  readonly type: GitObjectType,
  readonly name: string,
  readonly key: string,
}
