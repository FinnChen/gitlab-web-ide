/**
 * Thanks https://gitlab.com/gitlab-org/gitlab/-/merge_requests/34496
 */
import {getTreeChildSortKey} from '../utils/tree';
import {FileStatusType, IFileStatus} from '../types';
import {GitBlobObject, GitObject, GitObjectType, GitTreeObject, GitTreeObjectChild, GitTreeOrBlobObject} from './types';

const getFullPath = (path: string, name: string) => {
  return path ? `${path}/${name}` : name;
};

const createAddedDiffEntry = (path: string, newObjId: string): IFileStatus => {
  return {
    type: FileStatusType.Created,
    path,
    newObjId,
  };
};

const createModifiedDiffEntry = (objects: Map<string, GitObject<any>>, path: string, origObjId: string, newObjId: string): IFileStatus => {
  const origObj = objects.get(origObjId) as GitBlobObject;
  const newObj = objects.get(newObjId) as GitBlobObject;
  let origContentKey = undefined;

  if (!origObj.data.hasChangedFromLoaded && newObj.data.initContentHash) {
    origContentKey = newObj.data.initContentHash;
  }
  
  return {
    type: FileStatusType.Modified,
    path,
    origObjId,
    newObjId,
    origContentKey,
  };
};

const createRemovedDiffEntry = (path: string, origObjId: string): IFileStatus => {
  return {
    type: FileStatusType.Deleted,
    path,
    origObjId,
  };
};

const createAddedForAllBlobs = (objects: Map<string, GitObject<any>>, { type, name, key }: GitTreeObjectChild, path = ''): IFileStatus[] => {
  const fullPath = getFullPath(path, name);

  if (type === GitObjectType.BLOB) {
    return [createAddedDiffEntry(fullPath, key)];
  }

  const childObj = objects.get(key) as GitTreeObject;

  return childObj.data.children.flatMap(grandChild =>
    createAddedForAllBlobs(objects, grandChild, fullPath),
  );
};

const createRemovedForAllBlobs = (objects: Map<string, GitObject<any>>, { type, name, key }: GitTreeObjectChild, path = ''): IFileStatus[] => {
  const fullPath = getFullPath(path, name);

  if (type === GitObjectType.BLOB) {
    return [createRemovedDiffEntry(fullPath, key)];
  }

  const childObj = objects.get(key) as GitTreeObject;

  return childObj.data.children.flatMap(grandChild =>
    createRemovedForAllBlobs(objects, grandChild, fullPath),
  );
};

const calculateTreeDiff = (objects: Map<string, GitTreeOrBlobObject>, atree: GitTreeObject, btree: GitTreeObject, path = ''): IFileStatus[] => {
  const diff: IFileStatus[] = [];
  const alen = atree.data.children.length;
  const blen = btree.data.children.length;

  let aidx = 0;
  let bidx = 0;

  while (aidx < alen || bidx < blen) {
    const achild = atree.data.children[aidx];
    const bchild = btree.data.children[bidx];

    // if we're all out of childrens in the atree so add the bchild
    if (!achild) {
      diff.push(...createAddedForAllBlobs(objects, bchild, path));

      // next b child
      bidx += 1;
    }
    // if we're all out of children in the btree, then we must have removed this from the atree
    else if (!bchild) {
      diff.push(...createRemovedForAllBlobs(objects, achild, path));

      // next a child
      aidx += 1;
    } else {
      const aSortKey = getTreeChildSortKey(achild);
      const bSortKey = getTreeChildSortKey(bchild);

      if (aSortKey === bSortKey && achild.key === bchild.key) {
        // next children :)
        aidx += 1;
        bidx += 1;
      } else if (aSortKey === bSortKey) {
        const fullPath = getFullPath(path, achild.name);
        // if this is a blob, then we must have modified it
        if (achild.type === GitObjectType.BLOB) {
          diff.push(createModifiedDiffEntry(objects, fullPath, achild.key, bchild.key));
        } else {
          
          diff.push(
            ...calculateTreeDiff(objects, ensureAsTree(objects.get(achild.key)!), ensureAsTree(objects.get(bchild.key)!), fullPath),
          );
        }

        aidx += 1;
        bidx += 1;
      } else if (aSortKey < bSortKey) {
        // we deleted a child from a, so we have to add that and catch up the idx
        diff.push(...createRemovedForAllBlobs(objects, achild, path));
        aidx += 1;
      } else {
        // we must have added a child in b, so we have to add it and catch up the idx
        diff.push(...createAddedForAllBlobs(objects, bchild, path));
        bidx += 1;
      }
    }
  }

  return diff;
};

const ensureAsTree = (object: GitTreeOrBlobObject): GitTreeObject => {
  if (object.type === GitObjectType.TREE) {
    return object;
  }
  
  throw new Error(`Expected GitTreeObject, was: ${object.type}`)
}

export const calculateDiff = (objects: Map<string, GitObject<any>>, refA: string, refB: string): IFileStatus[] => {
  if (refA === refB) {
    return [];
  }

  const objA = ensureAsTree(objects.get(refA)!);
  const objB = ensureAsTree(objects.get(refB)!);

  return calculateTreeDiff(objects, objA, objB);
};
