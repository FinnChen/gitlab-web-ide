/**
 * Alot of thanks to https://gitlab.com/gitlab-org/gitlab/-/merge_requests/34496
 */
import {ROOT_PATH} from '../fsEntries';
import {IFileStatus, IFileSystem, IGit} from '../types';
import {clean} from './clean';
import {calculateDiff} from './diff';
import {updateObjects} from './fs';
import {GitBlobObject, GitObject, GitObjectRepo, GitTreeObject} from './types';

export class Git implements IGit, GitObjectRepo {
  private readonly _objects: Map<string, GitObject<any>>;

  private _lastTimestamp: number;
  private _headRef: string;
  private _fsRef: string;

  constructor() {
    this._objects = new Map<string, GitObject<any>>();
    this._headRef = '';
    this._fsRef = '';
    this._lastTimestamp = 0;
  }

  get headRef(): string {
    return this._headRef;
  }

  get fsRef(): string {
    return this._fsRef; 
  }

  readObject(id: string): GitBlobObject | GitTreeObject | undefined {
    return this._objects.get(id);
  }

  init(fs: IFileSystem) {
    const newRef = updateObjects({
      fs,
      lastTimestamp: this._lastTimestamp,
      objects: this._objects,
      rootRef: ''
    });

    this._fsRef = newRef;
    this._headRef = newRef;
    this._updateTimestamp(fs);
  }

  update(fs: IFileSystem) {
    this._fsRef = updateObjects({
      fs,
      lastTimestamp: this._lastTimestamp,
      objects: this._objects,
      rootRef: this._fsRef,
    });
    this._updateTimestamp(fs);
  }

  clean() {
    clean(this._objects, [this._fsRef, this._headRef]);
  }

  status(): IFileStatus[] {
    return calculateDiff(this._objects, this._headRef, this._fsRef);
  }

  private _updateTimestamp(fs: IFileSystem) {
    const entry = fs.getEntry(ROOT_PATH);

    if (!entry) {
      throw new Error(`[@gitlab/web-ide-vscode-extension] Expected to find root entry ${ROOT_PATH}`);
    }

    this._lastTimestamp = entry.mtime;
  }
}
