import { FileEntry } from '../types';
import { sha256Str, sha256 } from '../utils/sha256';
import { getTreeChildSortKey } from '../utils/tree';
import { GitBlobObject, GitBlobObjectData, GitObjectType, GitTreeObject, GitTreeObjectChild, GitObject, GitTreeObjectData } from './types';

const hasChangedFromLoaded = (file: FileEntry) => {
  if (!file.content || file.content?.type === 'unloaded') {
    return false;
  }

  return sha256(file.content.raw) !== file.initContentHash;
}

export const createGitObjectBlob = (file: FileEntry): GitBlobObject => {
  return {
    type: GitObjectType.BLOB,
    data: {
      hasChangedFromLoaded: hasChangedFromLoaded(file),
      content: file.content?.type === 'raw' ? file.content.raw : new Uint8Array([]),
      initContentHash: file.initContentHash,
    },
  };
};

const getBlobDataKey = ({ hasChangedFromLoaded, content }: GitBlobObjectData) => {
  if (hasChangedFromLoaded) {
    const contentStr = new TextDecoder().decode(content);

    return `changed ${contentStr}`;
  }

  return 'unchanged';
}

export const createGitObjectTree = (children: GitTreeObjectChild[]): GitTreeObject => {
  children.sort((a, b) => getTreeChildSortKey(a).localeCompare(getTreeChildSortKey(b)));

  return {
    type: GitObjectType.TREE,
    data: {
      children,
    }
  };
};

const getTreeDataKey = ({ children }: GitTreeObjectData) =>
  children.map(({ type, name, key }) => `${type} ${name} ${key}`).join(' ');

export const hashGitObject = ({ type, data }: GitObject<any>) => {
  const dataKey = type === GitObjectType.TREE ? getTreeDataKey(data) : getBlobDataKey(data);

  return sha256Str(`${type} ${dataKey}`);
};
