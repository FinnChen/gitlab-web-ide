import { GitObject, GitObjectType, GitTreeObject } from "./types";

const addAllRefsToSet = (objects: Map<string, GitObject<any>>, ref: string, keys: Set<string>) => {
  if (keys.has(ref)) {
    return;
  }

  keys.add(ref);
  const obj = objects.get(ref);

  if (!obj || obj.type === GitObjectType.BLOB) {
    return;
  }

  (obj as GitTreeObject).data.children.forEach(({ key }) => {
    addAllRefsToSet(objects, key, keys);
  });
};

export const clean = (objects: Map<string, GitObject<any>>, knownRefs: string[]) => {
  const knownKeys = new Set<string>();
  
  knownRefs.forEach(ref => {
    addAllRefsToSet(objects, ref, knownKeys);
  });

  for(let key of objects.keys()) {
    if (!knownKeys.has(key)) {
      objects.delete(key);
    }
  }
};
